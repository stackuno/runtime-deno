# Deno on Docker

The Deno CLI tool a slim Debian Buster image.

## Usage

In order to build the Docker image, run the following command:

```bash
make image
```

which installs the deno crate within a Rust container and then copies the
wanted deno binary into the target image.


Spawn a bash shell with the `deno` command in its PATH by running  `make bash`
and then run `deno` commands to your heart's desire.
